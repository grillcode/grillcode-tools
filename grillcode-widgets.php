<?php
/*
Plugin Name: Grillcode Widgets
Plugin URI: http://grillcode.com
Description: Grillcode Basic widgets
Version: 1.0
Author: Javier Otero
Author URI: http://grillcode.com
License: GPLv2
*/
 
 add_action ( 'widgets_init', 'gc_register_widgets' );
 function gc_register_widgets() {
 	 register_widget ( 'gc_widget_button' );
 }
 
 add_action('init', array($this, 'load_plugin_textdomain'));
 
 class gc_widget_button extends WP_Widget {
	function gc_widget_button() {
		parent::WP_Widget ( 'gcwidgetbutton', $name = 'gc_widget_button' );
	
		$widget_ops = array (
			'classname' => 'gc-button',
			'description' => 'Shows a button'
		);

		$control_ops = array (
			'width' => 250,
			'height' => 250,
			'id_base' => 'gc-button-widget' 
		);

		$this->WP_Widget ( 'gc-button-widget', 'GC Button', $widget_ops, $control_ops );
	}
	
	function form ( $instance ) {
		$defaults = array ( 'title'=>'', 'button_text'=>'', 'button_url'=>'' );
		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title', 'grillcode' ); ?>:</label>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name( 'title' ) ?>" id="<?php echo $this->get_field_id ( 'title' ) ?> " value="<?php echo $instance['title'] ?>" size="20"> 
		</p>
		<p>
			<label for="<?php echo $this->get_field_id('button_text'); ?>"><?php _e( 'Button text', 'grillcode' ); ?>:</label>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name('button_text') ?>" id="<?php echo $this->get_field_id ( 'button_text' ) ?> " value="<?php echo $instance['button_text'] ?>" size="20">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id ( 'button_url' ); ?>"><?php _e( 'Button URL', 'grillcode' ); ?>:</label>
			<input class="widefat" type="text" name="<?php echo $this->get_field_name ( 'button_url' ) ?>" id="<?php echo $this->get_field_id ( 'button_url' ) ?> " value="<?php echo $instance['button_url'] ?>" size="20">
		</p>

		<?php
		
	}
	function update ( $new_instance, $old_instance ) {
		// processes widget options to be saved
		$instance = $old_instance;
		  
		$instance['title'] 			= sanitize_text_field ( $new_instance['title'] );
		$instance['button_text'] 	= sanitize_text_field ( $new_instance['button_text'] );
		$instance['button_url'] 	= sanitize_text_field ( $new_instance['button_url'] );

		return $instance;
	}
	function widget ( $args, $instance) {
		// outputs the content of the widget
		extract ( $args );

		isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$title 					= isset( $instance['title'] ) ? esc_attr( $instance['title'] ) : '';
		$button_text 			= isset( $instance['button_text'] ) ? esc_attr( $instance['button_text'] ) : '';
		$button_url 			= isset( $instance['button_url'] ) ? esc_attr( $instance['button_url'] ) : '';;
		$out = '';

		if( $button_url!='' && $button_text!=''){
			$out .= '<a class="boton" href="' . $button_url . '">' . $button_text . '</a>';
		}		

		echo $before_widget;
		echo $before_title.$title.$after_title;
		echo $out;
		echo $after_widget;	
	
	}
}
